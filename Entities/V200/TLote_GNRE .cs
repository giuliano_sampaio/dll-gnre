﻿using SefazDLL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Entities.V200
{
    public class TLote_GNRE : IEntity
    {
        protected List<TDadosGNRE> guias =  new List<TDadosGNRE>();
        
        public void SetCollection(List<TDadosGNRE> collection)
        {
            this.guias = collection;
        }

        public void AddItem(TDadosGNRE guia)
        {
            this.guias.Add(guia);
        }
    }
}
