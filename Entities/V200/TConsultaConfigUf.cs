﻿using SefazDLL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Entities.V200
{
    public class TConsultaConfigUf : IEntity
    {
        public int ambiente;
        public string uf;
        public int receita;
    }
}
