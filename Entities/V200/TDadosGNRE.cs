﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Entities.V200
{
    public class TDadosGNRE
    {
        public string ufFavorecida;
        public string tipoGnre;
        public TEmpresa contribuinteEminente;
        public List<TItemGnre> itensGnre;
    }
}
