﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Entities.V200
{
    public class TItemGnre
    {
        public string receita;
        public string detalhamentoReceita;
        public TDocumentoOrigem documentoOrigem;
        public string produto;
        public List<TReferencia> referencia;
        public string dataVencimento;
        public string valor;
        public string convenio;
        public TContribuinteDestinatario contribuinteDestinatario;
        public List<TCampoExtra> camposExtras;
        public string numeroControle;
        public string numeroControleFecp;
    }
}
