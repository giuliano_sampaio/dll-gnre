﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Entities.V200
{
    public class TEmpresa
    {
        public string identificacao;
        public string razaoSocial;
        public string endereco;
        public string municipio;
        public string uf;
        public string cep;
        public string telefone;

    }
}
