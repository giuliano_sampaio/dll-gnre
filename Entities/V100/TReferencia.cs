﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Entities.V100
{
    public class TReferencia
    {
        public string periodo;
        public string mes;
        public string ano;
        public string parcela;
    }
}
