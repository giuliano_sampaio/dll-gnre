﻿using SefazDLL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Entities.V100
{
    public class TLote_GNRE : IEntity
    {
        public List<TDadosGNRE> guias =  new List<TDadosGNRE>();

        public void SetCollection(List<TDadosGNRE> guias)
        {
            this.guias = guias;
        }

        public void AddItem(TDadosGNRE guia)
        {
            this.guias.Add(guia);
        }
    }
}
