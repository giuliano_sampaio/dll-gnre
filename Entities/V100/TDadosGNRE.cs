﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Entities.V100
{
    public class TDadosGNRE
    {
        public string c01_UfFavorecida;
        public string c02_receita;
        public string c25_detalhamentoReceita;
        public string c26_produto;
        public string c27_tipoIdentificacaoEmitente;
        public TContribuinte c03_idContribuinteEmitente;
        public string c28_tipoDocOrigem;
        public string c04_docOrigem;
        public string c06_valorPrincipal;
        public string c10_valorTotal;
        public string c14_dataVencimento;
        public string c15_convenio;
        public string c16_razaoSocialEmitente;
        public string c17_inscricaoEstadualEmitente;
        public string c18_enderecoEmitente;
        public string c19_municipioEmitente;
        public string c20_ufEnderecoEmitente;
        public string c21_cepEmitente;
        public string c22_telefoneEmitente;
        public string c34_tipoIdentificacaoDestinatario;
        public TContribuinte c35_idContribuinteDestinatario;
        public string c36_inscricaoEstadualDestinatario;
        public string c37_razaoSocialDestinatario;
        public string c38_municipioDestinatario;
        public string c33_dataPagamento;
        public TReferencia c05_referencia;
        public List<TCampoExtra> c39_camposExtras;
    }
}
