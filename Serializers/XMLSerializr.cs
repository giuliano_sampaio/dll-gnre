﻿using SefazDLL.Contracts;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace SefazDLL.Serializers
{
    class XMLSerializr
    {
        /**
         * Recebe um objeto e transforma em XML para ser enviado
         * aos web services de GNRE
         */
        public static XmlDocument Serialize(String name, IEntity input)
        {
            Type type = input.GetType();
            XmlDocument xd = null;
            XmlSerializer ser = new XmlSerializer(type, name);
            MemoryStream memStm = new MemoryStream();

            using (memStm)
            {
                ser.Serialize(memStm, input);
                memStm.Position = 0;

                XmlReaderSettings settings = new XmlReaderSettings
                {
                    IgnoreWhitespace = true
                };

                using (var xtr = XmlReader.Create(memStm, settings))
                {
                    xd = new XmlDocument();
                    xd.Load(xtr);
                }
            }

            return xd;
        }

        public static Object Deserialize(XmlNode xml, Type type)
        {          
            XmlSerializer ser = new XmlSerializer(type);

            return ser.Deserialize(new XmlNodeReader(xml)); ;
        }
    }
}
