﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Certificates
{
    public class CertificatesStore
    {
        static protected StoreName name;
        static protected StoreLocation location;
        
        public void SetLocation(StoreLocation value)
        {
            location = value;
        }

        public void SetName(StoreName value)
        {
            name = value;
        }


        /// <summary>
        /// Retorna o local que se deve buscar o certificado       
        /// Se nada for definido irá buscar pela máquina local,
        /// por padrão
        /// </summary>
        /// <returns></returns>
        static public StoreLocation GetLocation()
        {
            if (location == 0)
            {
                location = StoreLocation.LocalMachine;
            }

            return location;

        }

        /**
         * Retorna o usuário que se deve buscar o certificado.
         * Se nada for definido irá buscar pelo usuário root, 
         * por padrão
         */
        static public StoreName GetStoreName()
        {
            if (name == 0)
            {
                return StoreName.Root;
            }

            return name;
        }

        /**
         * Retorna um certificado instalado 
         * de acordo com o thumbprint passado
         */
        public static X509Certificate2 GetCertificate(String print, String password)
        {
            X509Certificate2 search = FindByThumbPrint(print);

            if (search == null)
            {
                throw new Exception(Properties.Errors.Default.CertificateNotFound);
            }

            var raw = search.GetRawCertData();

            return new X509Certificate2(raw, password);
        }

        /**
         * Verifica no ambiente se existe alguma credencial
         * de acordo com o thumbprint do certificado
         * 
         * @param string print
         * */
        public static X509Certificate2 FindByThumbPrint(string print)
        {
            var flag  = OpenFlags.ReadOnly;
            var type  = X509FindType.FindByThumbprint;          
            var store = new X509Store(GetStoreName(), GetLocation());

            store.Open(flag);

            try
            {
                var results = store.Certificates.Find(type, print, true);
                return results[0];
            }
            finally
            {
                store.Close();
            }
        }
    }
}
