﻿using SefazDLL.Entities.V100;
using SefazDLL.Responses;
//using SefazDLL.TestGnreConfigUF;
using SefazDLL.br.gov.pe.testegnre.www;
using System;
using System.IO;
using System.ServiceModel;
using System.Xml;
using System.Xml.Serialization;

namespace SefazDLL.Services
{
    class ConfigUFService : FoundationService
    {
        /// <summary>
        /// Executa a função do web service e retorna a resposta
        /// em um objeto
        /// </summary>
        /// <param name="ambiente"></param>
        /// <param name="uf"></param>
        /// <param name="receita"></param>
        /// <returns></returns>
        public string Request(string ambiente, string uf, string receita)
        {
            var client = this.GetRequest();
            var address = this.GetEndpoint();  
            var header = this.GetHeader();
            var certificate = this.GetCertificate();
            var body = this.GetXml(ambiente, uf, receita);

            //client.Endpoint.Address = address;
            //client.ClientCredentials.ClientCertificate.Certificate = certificate;

            client.ClientCertificates.Add(certificate);
            client.gnreCabecMsgValue = header;
            

            try
            {       
                Type type = typeof(TConfigUf);
                XmlNode xml = client.consultar(body);

                return xml.OuterXml;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        
        protected XmlDocument GetXml(string ambiente, string uf, string receita)
        {
            TConsultaConfigUf input = new TConsultaConfigUf
            {
                ambiente = Convert.ToInt32(ambiente),
                uf = uf,
                receita = Convert.ToInt32(receita),
            };

            return this.Serialize(input);
        }

        protected XmlDocument Serialize(TConsultaConfigUf input)
        {
            Type type = input.GetType();
            XmlDocument xd = null;
            string nameSpace = Properties.Service.Default.NameSpace;
            XmlSerializer ser = new XmlSerializer(type, nameSpace);
            MemoryStream memStm = new MemoryStream();

            using (memStm)
            {
                ser.Serialize(memStm, input);
                memStm.Position = 0;

                XmlReaderSettings settings = new XmlReaderSettings
                {
                    IgnoreWhitespace = true
                };

                using (var xtr = XmlReader.Create(memStm, settings))
                {
                    xd = new XmlDocument();
                    xd.Load(xtr);
                }
            }

            return xd;
        }

        /// <summary>
        /// Retorna o endereço do web service que será chamado
        /// </summary>
        /// <returns></returns>
        protected EndpointAddress GetEndpoint()
        {
            string uri = Properties.Service.Default.UrlConfigUF;
            return new EndpointAddress(uri);
        }

        /// <summary>
        /// Retorna o cabeçalho com a vesão de dados
        /// para interpretação do xml
        /// </summary>
        /// <returns></returns>
        protected gnreCabecMsg GetHeader()
        {
            return new gnreCabecMsg
            {
                versaoDados = this.GetVersaoDados()
            };
        }

        /// <summary>
        ///     Retorna o objeto que fará o request para o web service
        /// </summary>
        /// <returns>
        ///     Objeto com a requisição
        /// </returns>
        //protected GnreConfigUFSoapClient GetRequest()
        //{
        //    GnreConfigUFSoapClient client = new GnreConfigUFSoapClient();
        //    return client;
        //}

        protected GnreConfigUF GetRequest()
        {
            GnreConfigUF client = new GnreConfigUF();
            return client;
        }
    }
}
