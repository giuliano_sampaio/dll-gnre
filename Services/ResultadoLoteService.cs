﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using SefazDLL.ResultadoLoteReference;
using SefazDLL.Entities.V100;
using SefazDLL.Responses;

namespace SefazDLL.Services
{
    class ResultadoLoteService : FoundationService
    {
        public string Request(string ambiente, string numeroRecibo)
        {
            var client = this.GetRequest();
            var address = this.GetEndpoint();
            var header = this.GetHeader();
            var certificate = this.GetCertificate();
            var body = this.GetXml(ambiente, numeroRecibo);

            client.Endpoint.Address = address;
            client.ClientCredentials.ClientCertificate.Certificate = certificate;

            try
            {
                Type type = typeof(TResultLote_GNRE);
                XmlNode xml = client.consultar(header, body);
                
                return xml.OuterXml;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Retorna o endereço do web service que será chamado
        /// </summary>
        /// <returns></returns>
        protected EndpointAddress GetEndpoint()
        {
            string uri = Properties.Service.Default.UrlResultadoLote;
            return new EndpointAddress(uri);
        }

        /// <summary>
        /// Retorna o cabeçalho com a vesão de dados
        /// para interpretação do xml
        /// </summary>
        /// <returns></returns>
        protected gnreCabecMsg GetHeader()
        {
            return new gnreCabecMsg
            {
                versaoDados = this.GetVersaoDados()
            };
        }

        protected XmlDocument GetXml(string ambiente, string numeroRecibo)
        {
            TConsLote_GNRE input = new TConsLote_GNRE{
                ambiente = Convert.ToInt32(ambiente),
                numeroRecibo = numeroRecibo
            };

            return this.Serialize(input);
        }

        public GnreResultadoLoteSoapClient GetRequest()
        {
            GnreResultadoLoteSoapClient Client = new GnreResultadoLoteSoapClient();
            return Client;
        }
    }
}
