﻿using SefazDLL.Contracts;
using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using SefazDLL.Serializers;
using SefazDLL.Certificates;
using SefazDLL.LoteRecepcaoReference;

namespace SefazDLL.Services
{
    class FoundationService
    {
        protected string versaoDados;
        protected string certificatePassword; // "271369"
        protected string thumbPrint;

        /// <summary>
        /// Define a senha que será usada para autenticar o 
        /// certificado
        /// </summary>
        /// <param name="value">Senha do certificado</param>
        public void SetCertificatePassword(string password)
        {
            this.certificatePassword = password;
        }

        /// <summary>
        /// Define a impressão digital do certificado instalado
        /// </summary>
        /// <param name="print"></param>
        public void SetThumbPrint(string print)
        {
            this.thumbPrint = print;
        }

        /// <summary>
        /// Define a versão de dados que será interpretado
        /// a requisição com o xml
        /// </summary>
        /// <param name="versao"></param>
        public void SetVersaoDados(string versao)
        {
            this.versaoDados = versao;
        }

        /// <summary>
        /// Retorna a versão de dados que será utilizado na chamada.
        /// Caso não tenha sido definido nenhum valor, irá utilizar 
        /// o valor padrão da DLL 
        /// </summary>
        /// <returns></returns>
        protected string GetVersaoDados()
        {
            return this.versaoDados ?? Properties.Service.Default.VersaoDados;
        }

        /// <summary>
        /// Retorna o objeto do certificado instalado
        /// </summary>
        /// <returns></returns>
        protected X509Certificate2 GetCertificate()
        {
            string print = this.thumbPrint;
            string password = this.certificatePassword;
            return CertificatesStore.GetCertificate(print, password); 
        }

        
        public void SetProxy()
        {
            var proxy = new WebProxy("proxycenesp.resource.com.br:3128", true)
            {
                Credentials = new NetworkCredential("t.rodrigo.nakamura", "resource@321")
            };

            WebRequest.DefaultWebProxy = proxy;
        }

        protected XmlDocument Serialize(IEntity input)
        {
            string nameSpace = Properties.Service.Default.NameSpace;
            return XMLSerializr.Serialize(nameSpace, input);
        }

        protected Object Deserialize(XmlNode xmlDoc, Type type)
        {
            return XMLSerializr.Deserialize(xmlDoc, type);
        }
    }
}
