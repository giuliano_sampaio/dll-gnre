﻿using System;
using System.Xml;
using SefazDLL.LoteRecepcaoReference;
using SefazDLL.Entities.V100;
using System.ServiceModel;
using System.Collections.Generic;
using SefazDLL.Responses;

namespace SefazDLL.Services
{
    class LoteRecepcaoService: FoundationService
    {
        public string Request(string xmlPath)
        {
            var client = this.GetRequest();
            var address = this.GetEndpoint();
            var header = this.GetHeader();
            var certificate = this.GetCertificate();
            var body = this.LoadXml(xmlPath);

            client.Endpoint.Address = address;
            client.ClientCredentials.ClientCertificate.Certificate = certificate;

            try
            {             
                Type type = typeof(TRetLote_GNRE); 
                XmlNode response = client.processar(header, body);

                return response.OuterXml;

            } catch (Exception e)
            {
                return e.Message;  
            }            
        }

        protected XmlDocument LoadXml(string path)
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(path);

            return doc;
        }

        /// <summary>
        /// Transforma uma lista de guias em XML
        /// </summary>
        /// <param name="guias"></param>
        /// <returns></returns>
        protected XmlDocument GetXml(List<TDadosGNRE> guias)
        {
            TLote_GNRE input = new TLote_GNRE();

            input.SetCollection(guias);

            return this.Serialize(input);
        }

        /// <summary>
        /// Retorna o endereço do web service que será chamado
        /// </summary>
        /// <returns></returns>
        protected EndpointAddress GetEndpoint()
        {
            string uri = Properties.Service.Default.UrlLoteRecepcao;
            return new EndpointAddress(uri);
        }

        /// <summary>
        /// Retorna o cabeçalho com a vesão de dados
        /// para interpretação do xml
        /// </summary>
        /// <returns></returns>
        protected gnreCabecMsg GetHeader()
        {
            return new gnreCabecMsg
            {
                versaoDados = this.GetVersaoDados()
            };
        }

        /// <summary>
        ///     Retorna o objeto que fará o request para o web service
        /// </summary>
        /// <returns>
        ///     Objeto com a requisição
        /// </returns>
        public GnreLoteRecepcaoSoapClient GetRequest()
        {
            GnreLoteRecepcaoSoapClient Client = new GnreLoteRecepcaoSoapClient();
            return Client;
        }
    }
}