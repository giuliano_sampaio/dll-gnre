﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SefazDLL.Contracts;
using SefazDLL.Entities.V100;

namespace SefazDLL.Mocks
{
    public class LoteRecepcaoV1Mock
    {
        /// <summary>
        /// Retorna um objeto de guia com todos os dados já preenchidos, 
        /// apenas para fins de didáticos e testes
        /// </summary>
        /// <returns></returns>
        public static TDadosGNRE GetGuia()
        {
            TDadosGNRE guia = new TDadosGNRE()
            {
                c01_UfFavorecida = "SP",
                c02_receita = "100064",
                c04_docOrigem = "15",
                c06_valorPrincipal = "100.00",
                c10_valorTotal = "100.00",
                c16_razaoSocialEmitente = "Teste de envio",
                c21_cepEmitente = "18407020",
                c20_ufEnderecoEmitente = "SP",
                c15_convenio = "1",
                c14_dataVencimento = "2019-07-20", // AAAA-MM-DD
                c18_enderecoEmitente =  "Rua Armando da Costa, 430",
                c26_produto = "1",
                c17_inscricaoEstadualEmitente = "122379",
                c22_telefoneEmitente = "123123",
                c25_detalhamentoReceita = "100064",
                c19_municipioEmitente = "33600",
                c27_tipoIdentificacaoEmitente = "2",
                c28_tipoDocOrigem = "13",
                c33_dataPagamento = "2019-07-12",
                c34_tipoIdentificacaoDestinatario = "1",
                c36_inscricaoEstadualDestinatario = "123892",
                c38_municipioDestinatario = "06902",
                c37_razaoSocialDestinatario = "Werbsers",
                c03_idContribuinteEmitente = GetContribuinteEmitente(),
                c35_idContribuinteDestinatario = GetContribuinteDestinatario(),
                c05_referencia = GetReferencia()
            };

            return guia;
        }

        protected static TContribuinte GetContribuinteEmitente()
        {
            return new TContribuinte
            {
                CNPJ = "86850640000106",
                //CPF = "92252217030",
            };
        }

        protected static TContribuinte GetContribuinteDestinatario()
        {
            return new TContribuinte
            {
                CNPJ = "84903300000170",
                //CPF = "33145502000"
            };
        }

        protected static TReferencia GetReferencia()
        {
            return new TReferencia
            {
                ano = "2019",
                mes = "10",
                parcela = "1",
                periodo = "1"
            };
        }
    }
}
