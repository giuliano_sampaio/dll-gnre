﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SefazDLL.Entities.V200;

namespace SefazDLL.Mocks
{
    class LoteRecepcaoV2Mock
    {
        public TDadosGNRE GetGuia()
        {
            TDadosGNRE guia = new TDadosGNRE()
            {
                tipoGnre = "2",
                ufFavorecida = "SP",
                contribuinteEminente = this.GetEmpresa(),
                itensGnre = this.GetItem()
            };

            return guia;
        }

        protected TEmpresa GetEmpresa()
        {
            TEmpresa empresa = new TEmpresa()
            {
                cep = "18407020",
                endereco = "Rua Armando da Costa, 740",
                identificacao = "1",
                municipio = "Itapeva",
                razaoSocial = "Giuliano de Sampaio Macedo",
                telefone = "35215074",
                uf = "SP"
            };

            return empresa;
        }

        protected List<TItemGnre> GetItem()
        {
            TItemGnre item = new TItemGnre()
            {
                convenio = "123",
                dataVencimento = "12072019",
                detalhamentoReceita = "000009",
                valor = "100,20",
                produto = "1",
                receita = "100099",
                documentoOrigem = this.GetDocumentoOrigem(),
                camposExtras = this.GetCampoExtra(),
                numeroControle = "123",
                contribuinteDestinatario = this.GetContribuinte(),
                numeroControleFecp = "019020202",
                referencia = this.GetReferencia()
            };

            List<TItemGnre> list = new List<TItemGnre>();
            list.Add(item);

            return list;
        }

        protected TDocumentoOrigem GetDocumentoOrigem()
        {
            return new TDocumentoOrigem()
            {
                tipo = "13",
            };
        }

        protected List<TReferencia> GetReferencia()
        {
            TReferencia item = new TReferencia()
            {
                ano = "2019",
                mes = "07",
                parcela = "1",
                periodo = "setembro"
            };

            List<TReferencia> list = new List<TReferencia>();
            list.Add(item);

            return list;
        }

        protected List<TCampoExtra> GetCampoExtra()
        {
            TCampoExtra item = new TCampoExtra()
            {
                codigo = 86,
                valor = "1"
            };

            List<TCampoExtra> list = new List<TCampoExtra>();
            list.Add(item);

            return list;
        }

        protected TContribuinteDestinatario GetContribuinte()
        {
            return new TContribuinteDestinatario()
            {
                identificacao = "x",
                municipio = "Itapeva",
                razaoSocial = "Empresa X"
            };
        }
    }
}
