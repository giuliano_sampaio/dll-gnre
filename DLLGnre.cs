﻿using System;
using System.Collections.Generic;
using SefazDLL.Entities.V100;
using SefazDLL.Responses;
using SefazDLL.Services;

namespace SefazDLL
{
    public class DLLGnre
    {
        protected static string pswd { get; set; }
        protected static string print { get; set; }

        /// <summary>
        /// Define qual será o certificado que será utilizado na chamada ao GNRE<br/>
        /// O certificado deve estar previamente instalado na máquina do usuário 
        /// </summary>
        /// <param name="thumbPrint">
        /// O código da impressão digital do certificado instalado
        /// </param>
        /// <param name="password">
        /// Senha utilizada para autenticar o certificado
        /// </param>
        public static void SetCredentials(string thumbPrint, string password)
        {
            DLLGnre.pswd = password;
            DLLGnre.print = thumbPrint;
        }

        /// <summary>
        ///     Executa a função GNRELoteRecepcao do web service.
        ///     Faz uma transmissão de Lote de GNRE assíncrona. 
        ///     As guias só poderão ser da versão 1.0 
        /// </summary>
        /// <param name="xmlPath">
        ///     Caminho para o XML com as guias para processamento
        /// </param>
        public static string LoteRecepcao(string xmlPath)
        {
            LoteRecepcaoService Service = new LoteRecepcaoService();

            Service.SetThumbPrint(print);
            Service.SetCertificatePassword(pswd);

            return Service.Request(xmlPath);
        }

        /// <summary>
        /// Executa a função GNREResultadoLote do web service. <br/>
        /// </summary>
        /// <param name="ambiente"></param>
        /// <param name="numeroRecibo"></param>
        /// <returns></returns>
        public static string ResultadoLote(string ambiente, string numeroRecibo)
        {
            ResultadoLoteService Service = new ResultadoLoteService();

            Service.SetThumbPrint(print);
            Service.SetCertificatePassword(pswd);

            return Service.Request(ambiente, numeroRecibo);
        }

        /// <summary>
        ///     Executa a função GNREConsultaUF do web service. <br/>
        ///     Consulta configurações da UF no Portal GNRE. 
        /// </summary>
        /// <param name="ambiente">
        ///     Define qual é o ambiente: 1 - Produção, 2 - Homologação
        /// </param>
        /// <param name="uf">
        ///     Define qual UF que será consultada
        /// </param>
        /// <param name="receita">
        ///     Código da receita
        /// </param>
        public static string ConfigUF(string ambiente, string uf, string receita)
        {
            //try
            //{
                ConfigUFService Service = new ConfigUFService();

                Service.SetThumbPrint(print);
                Service.SetCertificatePassword(pswd);

                return Service.Request(ambiente, uf, receita);

            //} catch (Exception e) {
            //    return e.Message;
            //}
            
        }
    }
}
