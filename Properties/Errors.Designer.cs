﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SefazDLL.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.1.0.0")]
    internal sealed partial class Errors : global::System.Configuration.ApplicationSettingsBase {
        
        private static Errors defaultInstance = ((Errors)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Errors())));
        
        public static Errors Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Não foi possível encontrar o certificado com esse thumb print. Verifique se o cer" +
            "tificado foi instalado corretamente e o thumb print está correto.")]
        public string CertificateNotFound {
            get {
                return ((string)(this["CertificateNotFound"]));
            }
        }
    }
}
