﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Responses
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.gnre.pe.gov.br", IsNullable = false)]
    public partial class TResultLote_GNRE
    {

        private byte ambienteField;

        private uint numeroReciboField;

        private TResultLote_GNRESituacaoProcess situacaoProcessField;

        private string resultadoField;

        /// <remarks/>
        public byte ambiente
        {
            get
            {
                return this.ambienteField;
            }
            set
            {
                this.ambienteField = value;
            }
        }

        /// <remarks/>
        public uint numeroRecibo
        {
            get
            {
                return this.numeroReciboField;
            }
            set
            {
                this.numeroReciboField = value;
            }
        }

        /// <remarks/>
        public TResultLote_GNRESituacaoProcess situacaoProcess
        {
            get
            {
                return this.situacaoProcessField;
            }
            set
            {
                this.situacaoProcessField = value;
            }
        }

        /// <remarks/>
        public string resultado
        {
            get
            {
                return this.resultadoField;
            }
            set
            {
                this.resultadoField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
    public partial class TResultLote_GNRESituacaoProcess
    {

        private ushort codigoField;

        private string descricaoField;

        /// <remarks/>
        public ushort codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }

        /// <remarks/>
        public string descricao
        {
            get
            {
                return this.descricaoField;
            }
            set
            {
                this.descricaoField = value;
            }
        }
    }


}
