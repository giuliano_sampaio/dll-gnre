﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Responses
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.gnre.pe.gov.br", IsNullable = false)]
    public partial class TRetLote_GNRE
    {

        private byte ambienteField;

        private TRetLote_GNRESituacaoRecepcao situacaoRecepcaoField;

        private TRetLote_GNRERecibo reciboField;

        /// <remarks/>
        public byte ambiente
        {
            get
            {
                return this.ambienteField;
            }
            set
            {
                this.ambienteField = value;
            }
        }

        /// <remarks/>
        public TRetLote_GNRESituacaoRecepcao situacaoRecepcao
        {
            get
            {
                return this.situacaoRecepcaoField;
            }
            set
            {
                this.situacaoRecepcaoField = value;
            }
        }

        /// <remarks/>
        public TRetLote_GNRERecibo recibo
        {
            get
            {
                return this.reciboField;
            }
            set
            {
                this.reciboField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
    public partial class TRetLote_GNRESituacaoRecepcao
    {

        private byte codigoField;

        private string descricaoField;

        /// <remarks/>
        public byte codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }

        /// <remarks/>
        public string descricao
        {
            get
            {
                return this.descricaoField;
            }
            set
            {
                this.descricaoField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
    public partial class TRetLote_GNRERecibo
    {

        private uint numeroField;

        private string dataHoraReciboField;

        private ushort tempoEstimadoProcField;

        /// <remarks/>
        public uint numero
        {
            get
            {
                return this.numeroField;
            }
            set
            {
                this.numeroField = value;
            }
        }

        /// <remarks/>
        public string dataHoraRecibo
        {
            get
            {
                return this.dataHoraReciboField;
            }
            set
            {
                this.dataHoraReciboField = value;
            }
        }

        /// <remarks/>
        public ushort tempoEstimadoProc
        {
            get
            {
                return this.tempoEstimadoProcField;
            }
            set
            {
                this.tempoEstimadoProcField = value;
            }
        }
    }


}
