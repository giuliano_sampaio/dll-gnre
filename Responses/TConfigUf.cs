﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SefazDLL.Responses
{

        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.gnre.pe.gov.br", IsNullable = false)]
        public partial class TConfigUf
        {
            private byte ambienteField;

            private string ufField;

            private TConfigUfSituacaoConsulta situacaoConsultaField;

            private TConfigUfExigeUfFavorecida exigeUfFavorecidaField;

            private TConfigUfExigeReceita exigeReceitaField;

            private TConfigUfReceitas receitasField;

            private decimal[] versoesXmlField;

            /// <remarks/>
            public byte ambiente
            {
                get
                {
                    return this.ambienteField;
                }
                set
                {
                    this.ambienteField = value;
                }
            }

            /// <remarks/>
            public string uf
            {
                get
                {
                    return this.ufField;
                }
                set
                {
                    this.ufField = value;
                }
            }

            /// <remarks/>
            public TConfigUfSituacaoConsulta situacaoConsulta
            {
                get
                {
                    return this.situacaoConsultaField;
                }
                set
                {
                    this.situacaoConsultaField = value;
                }
            }

            /// <remarks/>
            public TConfigUfExigeUfFavorecida exigeUfFavorecida
            {
                get
                {
                    return this.exigeUfFavorecidaField;
                }
                set
                {
                    this.exigeUfFavorecidaField = value;
                }
            }

            /// <remarks/>
            public TConfigUfExigeReceita exigeReceita
            {
                get
                {
                    return this.exigeReceitaField;
                }
                set
                {
                    this.exigeReceitaField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitas receitas
            {
                get
                {
                    return this.receitasField;
                }
                set
                {
                    this.receitasField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("versao", IsNullable = false)]
            public decimal[] versoesXml
            {
                get
                {
                    return this.versoesXmlField;
                }
                set
                {
                    this.versoesXmlField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfSituacaoConsulta
        {

            private ushort codigoField;

            private string descricaoField;

            /// <remarks/>
            public ushort codigo
            {
                get
                {
                    return this.codigoField;
                }
                set
                {
                    this.codigoField = value;
                }
            }

            /// <remarks/>
            public string descricao
            {
                get
                {
                    return this.descricaoField;
                }
                set
                {
                    this.descricaoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfExigeUfFavorecida
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfExigeReceita
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitas
        {

            private TConfigUfReceitasReceita receitaField;

            /// <remarks/>
            public TConfigUfReceitasReceita receita
            {
                get
                {
                    return this.receitaField;
                }
                set
                {
                    this.receitaField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceita
        {

            private string exigeContribuinteEmitenteField;

            private TConfigUfReceitasReceitaExigeDetalhamentoReceita exigeDetalhamentoReceitaField;

            private TConfigUfReceitasReceitaExigeProduto exigeProdutoField;

            private TConfigUfReceitasReceitaExigePeriodoReferencia exigePeriodoReferenciaField;

            private TConfigUfReceitasReceitaExigePeriodoApuracao exigePeriodoApuracaoField;

            private TConfigUfReceitasReceitaExigeParcela exigeParcelaField;

            private TConfigUfReceitasReceitaValorExigido valorExigidoField;

            private TConfigUfReceitasReceitaExigeDocumentoOrigem exigeDocumentoOrigemField;

            private decimal[] versoesXmlDocOrigemField;

            private string exigeContribuinteDestinatarioField;

            private TConfigUfReceitasReceitaExigeDataVencimento exigeDataVencimentoField;

            private TConfigUfReceitasReceitaExigeDataPagamento exigeDataPagamentoField;

            private TConfigUfReceitasReceitaExigeConvenio exigeConvenioField;

            private TConfigUfReceitasReceitaExigeValorFecp exigeValorFecpField;

            private TConfigUfReceitasReceitaExigeCamposAdicionais exigeCamposAdicionaisField;

            private uint codigoField;

            private string descricaoField;

            /// <remarks/>
            public string exigeContribuinteEmitente
            {
                get
                {
                    return this.exigeContribuinteEmitenteField;
                }
                set
                {
                    this.exigeContribuinteEmitenteField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigeDetalhamentoReceita exigeDetalhamentoReceita
            {
                get
                {
                    return this.exigeDetalhamentoReceitaField;
                }
                set
                {
                    this.exigeDetalhamentoReceitaField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigeProduto exigeProduto
            {
                get
                {
                    return this.exigeProdutoField;
                }
                set
                {
                    this.exigeProdutoField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigePeriodoReferencia exigePeriodoReferencia
            {
                get
                {
                    return this.exigePeriodoReferenciaField;
                }
                set
                {
                    this.exigePeriodoReferenciaField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigePeriodoApuracao exigePeriodoApuracao
            {
                get
                {
                    return this.exigePeriodoApuracaoField;
                }
                set
                {
                    this.exigePeriodoApuracaoField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigeParcela exigeParcela
            {
                get
                {
                    return this.exigeParcelaField;
                }
                set
                {
                    this.exigeParcelaField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaValorExigido valorExigido
            {
                get
                {
                    return this.valorExigidoField;
                }
                set
                {
                    this.valorExigidoField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigeDocumentoOrigem exigeDocumentoOrigem
            {
                get
                {
                    return this.exigeDocumentoOrigemField;
                }
                set
                {
                    this.exigeDocumentoOrigemField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("versao", IsNullable = false)]
            public decimal[] versoesXmlDocOrigem
            {
                get
                {
                    return this.versoesXmlDocOrigemField;
                }
                set
                {
                    this.versoesXmlDocOrigemField = value;
                }
            }

            /// <remarks/>
            public string exigeContribuinteDestinatario
            {
                get
                {
                    return this.exigeContribuinteDestinatarioField;
                }
                set
                {
                    this.exigeContribuinteDestinatarioField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigeDataVencimento exigeDataVencimento
            {
                get
                {
                    return this.exigeDataVencimentoField;
                }
                set
                {
                    this.exigeDataVencimentoField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigeDataPagamento exigeDataPagamento
            {
                get
                {
                    return this.exigeDataPagamentoField;
                }
                set
                {
                    this.exigeDataPagamentoField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigeConvenio exigeConvenio
            {
                get
                {
                    return this.exigeConvenioField;
                }
                set
                {
                    this.exigeConvenioField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigeValorFecp exigeValorFecp
            {
                get
                {
                    return this.exigeValorFecpField;
                }
                set
                {
                    this.exigeValorFecpField = value;
                }
            }

            /// <remarks/>
            public TConfigUfReceitasReceitaExigeCamposAdicionais exigeCamposAdicionais
            {
                get
                {
                    return this.exigeCamposAdicionaisField;
                }
                set
                {
                    this.exigeCamposAdicionaisField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public uint codigo
            {
                get
                {
                    return this.codigoField;
                }
                set
                {
                    this.codigoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string descricao
            {
                get
                {
                    return this.descricaoField;
                }
                set
                {
                    this.descricaoField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigeDetalhamentoReceita
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigeProduto
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigePeriodoReferencia
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigePeriodoApuracao
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigeParcela
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaValorExigido
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigeDocumentoOrigem
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigeDataVencimento
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigeDataPagamento
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigeConvenio
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigeValorFecp
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gnre.pe.gov.br")]
        public partial class TConfigUfReceitasReceitaExigeCamposAdicionais
        {

            private string campoField;

            private string valueField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string campo
            {
                get
                {
                    return this.campoField;
                }
                set
                {
                    this.campoField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }
}
